'use strict';
const db=uniCloud.database()
//聚合的操作符
const $=db.command.aggregate
exports.main = async (event, context) => {
	const collection= db.collection("article")
	// let res=await collection.field({
	// 	//true 表示只返回这个字段，false表示不返回这个字段
	// 	content:false
	// }).get()
	let {
		user_id,
		classify,
		page=1,
		pageSize=2
	}=event
	let matchObj={}
	if(classify==="全部"){
		
	}else{
		matchObj={classify:classify}
	}
	
	//聚和的操作，更精细化的分组，求和、指定字段
	// let list=await collection.aggregate().match({
	// 	classify:classify
	// })
	let userInfo=await db.collection("user").doc(user_id).get()
	let article_likes_ids=userInfo.data[0].article_likes_ids
	
	let list=await collection.aggregate()
	//追加字段
	.addFields({
		is_like:$.in(["$_id",article_likes_ids])
	})
	.match(matchObj)
	.project({//作用类似用field，指定某些字段返回，某写字段不返回
		content:false//或者0
	})
	.skip(pageSize*(page-1))//要跳过多少条数据
	.limit(pageSize)//每页限制显示多少条数据
	.end()
	//返回数据给客户端
	return {
		code:200,
		msg:"数据请求成功",
		data:list.data
	}
};
