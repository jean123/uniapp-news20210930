'use strict';
const db=uniCloud.database()
//是对数组、字符串、数字的操作
const dbCmd=db.command
exports.main = async (event, context) => {
	const {
		user_id,
		article_id
	}=event
	let userInfo=await db.collection("user").doc(user_id).get()		
	let article_likes_ids=userInfo.data[0].article_likes_ids
	
	let dbCmdFuns=null
	if(article_likes_ids.includes(article_id)){
		dbCmdFuns=dbCmd.pull(article_id)//删除
	}else{
		dbCmdFuns=dbCmd.addToSet(article_id)//追加
	}
	let res=await db.collection("user").doc(user_id).update({
		article_likes_ids:dbCmdFuns
	})
	// let res=await db.collection("user").doc(user_id).update({
	// 	article_likes_ids:dbCmd.addToSet(article_id)
	// })
	// let res=await db.collection("user").doc(user_id).update({
	// 	article_likes_ids:dbCmd.pull(article_id)
	// })
	//返回数据给客户端
	return res
};
